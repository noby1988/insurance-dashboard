import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule,
         ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule,
         MatToolbarModule,
         MatIconModule,
         MatCardModule,
         MatFormFieldModule,
         MatInputModule,
         MatDatepickerModule,
         MatNativeDateModule,
         MatTooltipModule } from '@angular/material';  
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { MessageDialogComponent } from './components/home/popups/message-dialog/message-dialog.component';
import { MessageInputDialogComponent } from './components/home/popups/message-input-dialog/message-input-dialog.component';
import { CommandDialogComponent } from './components/home/popups/command-dialog/command-dialog.component';
import { LoggedInAuthguard } from './services/logged-in-authguard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MessageDialogComponent,
    MessageInputDialogComponent,
    CommandDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule
  ],
  entryComponents: [
    MessageDialogComponent,
    MessageInputDialogComponent,
    CommandDialogComponent
  ],
  providers: [
    LoggedInAuthguard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
