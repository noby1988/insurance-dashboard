import { Injectable, ɵConsole } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import * as io from 'socket.io-client';
import { SocketClientUserService } from './socket-client-user.service';

export class Message {
  author: string;
  message: string;
}

export class Command {
}

@Injectable({
  providedIn: 'root'
})
export class SocketClientChatService {

  // Client socket
  private clientSocket;
  // Subject to emit custom events
  private chatEventSubject: Subject<any>;
  // Array to hold logs
  public changeLog: Array<any>;
  // Array to hold already processed command types
  public selectedOptions: Array<string>;

  constructor(private userService: SocketClientUserService) {
    this.chatEventSubject = new Subject<any>();
    this.changeLog = [];
    this.selectedOptions = [];
  }

  // Method to initialize the client socket and connect to server socket
  public initializeSocket() {
    this.clientSocket = io('https://demo-chat-server.on.ag');
    /* On each socket event, a custom event is published via
      chatEventSubject which is exposed as an Observable via receiveEvent()*/
    this.clientSocket.on('connect', ($data: any) => {
      console.log('Connected to server successfully');
      this.publishEvent({ type: 'connect', data: $data });
    });
    this.clientSocket.on('command', ($data: any) => {
      this.publishEvent({ type: 'command', data: $data });
    });
    this.clientSocket.on('message', ($data: any) => {
      this.publishEvent({ type: 'message', data: $data });
    });
    this.clientSocket.on('disconnect', ($data: any) => {
      console.log('Disconnected from server successfully');
      this.publishEvent({ type: 'disconnect', data: $data });
    });
  }

  // Method to send data to server
  public sendData(key: string, input: Message | Command) {
    this.clientSocket.emit(key, input);
  }

  // Method to publish custom event
  public publishEvent($event: any) {
    this.chatEventSubject.next($event);
  }

  // Method to expose subject as an Observable for emitting events
  public receiveEvent(): Observable<any>  {
    return this.chatEventSubject.asObservable();
  }

  // Method to disconnect from socket
  public disconnectSocket() {
    this.clientSocket.disconnect();
  }

  // Method to update log
  public log(type: string, logMsg: string) {
    this.changeLog.push({
      author: (type === 'usr') ?
        this.userService.getUsername() : 'Ottonova Bot',
      message: logMsg
    });
    this.publishEvent({ type: 'scrollToBottom' });
  }

}
