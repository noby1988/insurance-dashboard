import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { SocketClientUserService } from './socket-client-user.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInAuthguard implements CanActivate {

  constructor(private userService: SocketClientUserService, private router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userService.getUsername()) {
      return true;
    } else {
      // Redirect to login page if user is not logged in
      this.router.navigate(['/login']);
    }
  } 
  
}
