import { Component } from '@angular/core';
import { MatDialogRef} from '@angular/material/dialog';
import { SocketClientChatService } from 'src/app/services/socket-client-chat.service';
import { SocketClientUserService } from 'src/app/services/socket-client-user.service';

@Component({
  selector: 'app-message-input-dialog',
  templateUrl: './message-input-dialog.component.html',
  styleUrls: ['./message-input-dialog.component.scss']
})
export class MessageInputDialogComponent {

  public pingText: string;

  constructor(private chatService: SocketClientChatService,
              private userService: SocketClientUserService,
              public dialogRef: MatDialogRef<MessageInputDialogComponent>) {
    this.pingText = '';
  }

  public pingBot() {
    // Update Change log
    this.chatService.log('usr', this.pingText);
    // Method to send user entered data to server socket
    this.chatService.sendData('message', {
      author: this.userService.getUsername(),
      message: this.pingText
    });
    this.dialogRef.close();
  }

}
