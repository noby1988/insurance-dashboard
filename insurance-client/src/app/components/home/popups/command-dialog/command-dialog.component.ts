import { Component, Inject, ViewChildren, AfterViewChecked } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SocketClientChatService } from 'src/app/services/socket-client-chat.service';
import {} from 'googlemaps';

@Component({
  selector: 'app-command-dialog',
  templateUrl: './command-dialog.component.html',
  styleUrls: ['./command-dialog.component.scss']
})
export class CommandDialogComponent {

  /* This is a generic dialog component for all the command
    responses received from server. */

  // [when command.type='rate'] Variable to bind the selected rating
  private selectedRating: number;
  // [when command.type='rate'] Array to hold the stars in rating selector
  public ratingArray: Array<number>;
  // [when command.type='date'] Variable to restrict previous dates in datepicker
  public minDate: Date;
  // [when command.type='date'] Variable to bind the selected date from datepicker
  public selectedDate: Date;
  // [when command.type='date'] Array to hold the day selector
  public dayArray: Array<string>;
  // [when command.type='date'] Variable to bind the selected day from day selector
  public selectedDay: string;
  // DOM hook to get reference of the map element
  @ViewChildren('map') mapElement: any;
  public map: google.maps.Map;
  // [when command.type='complete'] Variable to bind the selected option from continue dialog
  public selectedOption: string;

  constructor(private chatService: SocketClientChatService,
              public dialogRef: MatDialogRef<CommandDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    // To disable closing popup when user clicks outside
    this.dialogRef.disableClose = true;
    // Socket response is accessed as 'this.data.data'
    if (this.data.type === 'rate') {
      // data.data[0] will be current rating
      this.selectedRating = data.data[0];
      // data.data[1] will be total rating
      this.ratingArray = [];
      // creating placeholders for rating
      for (let i = 0; i < data.data[1]; i++) {
        this.ratingArray.push(i);
      }
    } else if (this.data.type === 'date') {
      // data.data will be received date as string with time zone information
      const parsedDate: Date = new Date(data.data);
      // Extract year, month and day from parsed date to set min date for datepicker
      this.minDate = new Date(parsedDate.getFullYear(), parsedDate.getMonth(), parsedDate.getDate());
      // Defaulting selected date as minimum date
      this.selectedDate = this.minDate;
    } else if (this.data.type === 'map') {
      // data.data.lng and data.data.lat will be longitude and latitude
      // hence, input object to initialize google map 
      const mapProperties: any = {
        center: new google.maps.LatLng(this.data.data.lat, this.data.data.lng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      // A timer is set here to check when the dom hook for accessing map element is ready
      // ViewChildren is used here to access the dom element since its loaded on the fly
      const intervalID: any = setInterval(() => {
        if (this.mapElement) {
          // once hook is found, initialize it with input object and clear the timer
          clearInterval(intervalID);
          this.map = new google.maps.Map(this.mapElement._results[0].nativeElement, mapProperties);
        }
      }, 200);
    }
  }

  // Method to show day selector after selecting date
  private showDaySelector() {
    // Initialize the day selector array
    this.dayArray = [];
    // Updating the data.type as 'day', data.type is used for template switching
    // and this value is client side updated
    this.data.type = 'day';
    // Index Map for day selector
    const dayIndexMap: any = {
      0: 'Sunday',
      1: 'Monday',
      2: 'Tuesday',
      3: 'Wednesday',
      4: 'Thursday',
      5: 'Friday',
      6: 'Saturday'
    };
    let tempIndex: number = this.selectedDate.getDay();
    this.selectedDay = dayIndexMap[tempIndex];
    /* Based on the selected date's day number, day selector should be displayed.
      Say if selected day is a Wednesday, the day selector buttons should be displayed like
      Wednesday, Thursday, Friday, Saturday, Sunday, Monday, Tuesday.
      Below code generates the desired array */
    while (this.dayArray.length < 7) {
      this.dayArray.push(dayIndexMap[tempIndex]);
      tempIndex++;
      if (tempIndex === 7) {
        tempIndex = 0;
      }
    }
  }

  // Generic method to handle done event 
  public done(cmdType: string, options?: string) {
    switch(cmdType) {
      case 'map': {
        this.chatService.log('usr', 'confirmed the location');
        this.chatService.log('bot', 'Thank you for confirming location');
        break;
      }
      case 'date': {
        // Updating change log to show on home page
        this.chatService.log('usr', 'selected the date as :: ' + (this.selectedDate.getMonth() + 1)
          + '/' + this.selectedDate.getDate() + '/' + this.selectedDate.getFullYear());
        this.chatService.log('bot', 'Thank you for confirming the date');  
        this.chatService.log('bot', 'Please select a day');
        this.showDaySelector();
        break;
      }
      case 'day': {
        this.chatService.log('usr', 'selected the day as :: ' + this.selectedDay);
        this.chatService.log('bot', 'Thank you for confirming the day');
        break;
      }
      case 'rate': {
        this.chatService.log('usr', 'selected the rating as :: ' + this.selectedRating);
        break;
      }
      case 'complete': {
        this.chatService.log('usr', (options === 'Yes') ?
          'Yes, I want to continue' : 'No, I do not want to continue');
        // Reset the already processed options
        this.chatService.selectedOptions = [];
        if (options === 'No') {
          // Clear the logs
          this.chatService.changeLog = [];
          this.chatService.publishEvent({ type: 'logout' });
        }
        break;
      }
      default: break;
    }
    // After selecting date dialog should not be closed, since day selector needs to be displayed
    if (cmdType !== 'date') {
      this.dialogRef.close();
    }
  }

  // Method to decide star/empty star icon to be shown in rating bar
  public getRatingIcon(index: number) {
    if (this.selectedRating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

}
