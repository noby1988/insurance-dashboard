import { SocketClientUserService } from 'src/app/services/socket-client-user.service';
import { Component } from '@angular/core';
import { FormGroup,
         FormControl,
         Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public loginForm: FormGroup;

  constructor(private router: Router,
              private userService: SocketClientUserService) {
    this.loginForm = new FormGroup({
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required])
    });
  }

  // Executed When Form Is Submitted
  public onFormSubmit() {
    this.userService.setUsername(this.loginForm.controls.username.value);
    this.router.navigate(['/home']);
  }

}
